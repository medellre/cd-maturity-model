The Continuous Delivery Maturity Model version 2014.12 by Rene Medellin.

Contains statements of maturity across functional areas of the software delivery lifecycle to help measure agility and CD achievement.

This work is made available under a Creative Commons

    Attribution-NonCommercial-ShareAlike 4.0 International License

To view a copy of this license, visit:

 [http://creativecommons.org/licenses/by-nc-sa/4.0](http://creativecommons.org/licenses/by-nc-sa/4.0/)